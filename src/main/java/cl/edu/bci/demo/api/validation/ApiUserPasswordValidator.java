package cl.edu.bci.demo.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class ApiUserPasswordValidator implements ConstraintValidator<IApiUserPasswordConstraint, String>{

	@Value( "${password.constraint.regex}")
	private String passwordRegex; 
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value.matches(passwordRegex);
	}

}
