package cl.edu.bci.demo.api.exception;

public class ApiUserEmailDuplicatedException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public ApiUserEmailDuplicatedException(){
		super("El correo ya se ha registrado");
	}
}
