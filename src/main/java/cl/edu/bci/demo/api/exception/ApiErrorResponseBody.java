package cl.edu.bci.demo.api.exception;

public class ApiErrorResponseBody{
	
	private final String message;
	
	ApiErrorResponseBody(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
