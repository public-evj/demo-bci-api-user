package cl.edu.bci.demo.api.service.imp;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.edu.bci.demo.api.exception.ApiUserEmailDuplicatedException;
import cl.edu.bci.demo.api.exception.ApiUserNotFoundException;
import cl.edu.bci.demo.api.model.dto.ApiUserLoginRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserLoginResponseBody;
import cl.edu.bci.demo.api.model.dto.ApiUserRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserRequestBodyPhone;
import cl.edu.bci.demo.api.model.dto.ApiUserResponseBody;
import cl.edu.bci.demo.api.model.entity.ApiUserEntity;
import cl.edu.bci.demo.api.model.entity.ApiUserPhoneEntity;
import cl.edu.bci.demo.api.model.repository.IApiUserDao;
import cl.edu.bci.demo.api.service.IApiUserPhoneService;
import cl.edu.bci.demo.api.service.IApiUserService;

@Service
public class ApiUserService implements IApiUserService{
	
	@Autowired
	private IApiUserDao dao;
	
	@Autowired
	private IApiUserPhoneService phoneService;
	
	@Override
	public List<ApiUserEntity> findAll() {
		return (List<ApiUserEntity>) dao.findAll();
	}

	@Override
	public ApiUserResponseBody saveApiUserEntity(final ApiUserRequestBody user) {
		if(findByApiUserEmail(user.getEmail())!=null) {
			throw new ApiUserEmailDuplicatedException(); 
		}
		final ApiUserEntity userEntity = new ApiUserEntity();
		userEntity.setEmail(user.getEmail());
		userEntity.setName(user.getName());
		userEntity.setPassword(user.getPassword());
		userEntity.setPhones(new ArrayList<>());
		userEntity.setActive(true);
		userEntity.setToken(UUID.randomUUID());
		dao.save(userEntity);
		
		user.getPhones().stream().forEach(phone->{
			final ApiUserPhoneEntity phoneEntity = new ApiUserPhoneEntity();
			phoneEntity.setUserId(userEntity.getId());
			phoneEntity.setCountryCode(phone.getCountryCode());
			phoneEntity.setCityCode(phone.getCityCode());
			phoneEntity.setNumber(phone.getNumber());
			phoneService.saveApiUserPhoneEntity(phoneEntity);
			userEntity.getPhones().add(phoneEntity);
		});
		
		final ApiUserResponseBody response = new ApiUserResponseBody();
		response.setId(userEntity.getId());
		response.setActive(userEntity.isActive());
		response.setCreated(userEntity.getCreatedAt());
		response.setModified(userEntity.getModifiedAt());
		response.setLastLogin(userEntity.getLastLoginAt());
		response.setToken(userEntity.getToken().toString());
		response.setUser(user);
		return response;
	}

	@Override
	public ApiUserResponseBody findByApiUserId(final UUID id) {
		ApiUserEntity userEntity = dao.findById(id).orElse(null);
		
		if(userEntity==null) {
			throw new ApiUserNotFoundException();
		}
		
		final ApiUserResponseBody response = new ApiUserResponseBody();
		final ApiUserRequestBody user = new ApiUserRequestBody();
		
		user.setEmail(userEntity.getEmail());
		user.setName(userEntity.getName());
		user.setPassword(userEntity.getPassword());
		user.setPhones(new ArrayList<>());
		
		userEntity.getPhones().stream().forEach(phoneEntity->{
			final ApiUserRequestBodyPhone phone = new ApiUserRequestBodyPhone();
			phone.setCountryCode(phoneEntity.getCountryCode());
			phone.setCityCode(phoneEntity.getCityCode());
			phone.setNumber(phoneEntity.getNumber());
			user.getPhones().add(phone);
		});
		
		response.setUser(user);
		response.setActive(userEntity.isActive());
		response.setCreated(userEntity.getCreatedAt());
		response.setModified(userEntity.getModifiedAt());
		response.setLastLogin(userEntity.getLastLoginAt());
		response.setId(id);
		response.setToken(userEntity.getToken().toString());
		return response;
	}

	@Override
	public ApiUserLoginResponseBody login(ApiUserLoginRequestBody login) {
		ApiUserEntity userEntity = findByApiUserEmail(login.getEmail());
		
		if(userEntity == null || !userEntity.getPassword().equals(login.getPassword())) {
			throw new ApiUserNotFoundException();
		}
		userEntity.setLastLoginAt(new Date());
		dao.save(userEntity);
		
		return new ApiUserLoginResponseBody(userEntity.getId());
	}
	
	private ApiUserEntity findByApiUserEmail(final String email) {
		return dao.findByEmail(email).orElse(null);
	}

}
