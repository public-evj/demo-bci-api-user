package cl.edu.bci.demo.api.exception;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApiErrorExceptionHandler {
	
	@Value( "${password.constraint.error.message}")
	public String userPasswordErrorMessage;
	
	@ResponseStatus(HttpStatus.CONFLICT)		
	@ExceptionHandler(ApiUserEmailDuplicatedException.class)
	public ApiErrorResponseBody handleApiUserEmailDuplicatedException(ApiUserEmailDuplicatedException e) {
		return new ApiErrorResponseBody(e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)		
	@ExceptionHandler(ApiUserNotFoundException.class)
	public ApiErrorResponseBody handleApiUserNotFoundException(ApiUserNotFoundException e) {
		return new ApiErrorResponseBody(e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ApiErrorResponseBody handleValidationExceptions(
	  MethodArgumentNotValidException e) {
		ObjectError error;
	    if(!e.getBindingResult().getAllErrors().isEmpty()) {
	    	error = e.getBindingResult().getAllErrors().get(0);
	    	String fieldName = ((FieldError) error).getField();
	    	String errorMessage = error.getDefaultMessage();
	    	if(fieldName.equals("password")) {
	    		errorMessage = userPasswordErrorMessage;
	    	}
	        return new ApiErrorResponseBody(fieldName +" "+ errorMessage);
	    }else {
	    	return handleDefaultException(e);
	    }
	    
	}
		
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public ApiErrorResponseBody handleDefaultException(Exception e) {
		return new ApiErrorResponseBody(e.getMessage());
	}
}
