package cl.edu.bci.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;

@SpringBootApplication
public class DemoBciApiUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBciApiUserApplication.class, args);
	}
	
	@Bean
	  public OpenAPI openApiInfo() {
	      return new OpenAPI().info(
	            	new Info()
	            	.title("API RESTful Demo de Creación de Usuarios")
	            	.version("v1.0")
	              	.contact(new Contact().name("Eduardo Vergara").email("eduardovergarajara@gmail.com"))
	              	.description("Esta API es una demo, la cual sirve para registrar usuarios. Proyecto desarrollado por Eduardo Vergara Jara")
	              )
	    		  .addServersItem(new Server().url("http://localhost:8080/apiDemoBci").description("LOCAL"));
	  }

}
