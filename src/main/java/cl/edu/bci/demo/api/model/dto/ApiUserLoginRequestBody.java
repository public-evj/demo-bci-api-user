package cl.edu.bci.demo.api.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import cl.edu.bci.demo.api.validation.IApiUserPasswordConstraint;
import io.swagger.v3.oas.annotations.media.Schema;

public class ApiUserLoginRequestBody {
	
	@NotBlank
	@Email
	@Schema(description = "Email del Usuario", example = "juan@rodriguez.org", required = true)
	private String email;
	@NotBlank
	@IApiUserPasswordConstraint
	@Schema(description = "Password del Usuario", example = "Jr_4567890", required = true)
	private String password;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
