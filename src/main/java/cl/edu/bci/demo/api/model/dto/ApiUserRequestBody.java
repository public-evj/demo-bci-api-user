package cl.edu.bci.demo.api.model.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import cl.edu.bci.demo.api.validation.IApiUserPasswordConstraint;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

public class ApiUserRequestBody {
	
	@NotBlank
	@Schema(description = "Nombre del Usuario", example = "Juan Rodriguez", required = true)
	private String name;
	@NotBlank
	@Email
	@Schema(description = "Email del Usuario", example = "juan@rodriguez.org", required = true)
	private String email;
	@NotBlank
	@IApiUserPasswordConstraint
	@Schema(description = "Password del Usuario", example = "Jr_4567890", required = true)
	private String password;
	@ArraySchema(schema = @Schema(description = "Lista de teléfonos", required = false))
	private List<ApiUserRequestBodyPhone> phones = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<ApiUserRequestBodyPhone> getPhones() {
		return phones;
	}
	public void setPhones(List<ApiUserRequestBodyPhone> phones) {
		this.phones = phones;
	}
	
}
