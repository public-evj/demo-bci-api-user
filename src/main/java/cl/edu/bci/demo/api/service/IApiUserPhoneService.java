package cl.edu.bci.demo.api.service;

import java.util.List;
import java.util.UUID;

import cl.edu.bci.demo.api.model.entity.ApiUserPhoneEntity;

public interface IApiUserPhoneService {

	public void saveApiUserPhoneEntity(ApiUserPhoneEntity phone);
	
	public List<ApiUserPhoneEntity> findByApiUserId(UUID id);
	
}
