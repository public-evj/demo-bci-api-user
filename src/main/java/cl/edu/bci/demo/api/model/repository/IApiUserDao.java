package cl.edu.bci.demo.api.model.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import cl.edu.bci.demo.api.model.entity.ApiUserEntity;

public interface IApiUserDao extends CrudRepository<ApiUserEntity, UUID> {
	
	public Optional<ApiUserEntity> findByEmail(final String email);
}
