package cl.edu.bci.demo.api.service.imp;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.edu.bci.demo.api.model.entity.ApiUserPhoneEntity;
import cl.edu.bci.demo.api.model.repository.IApiUserPhoneDao;
import cl.edu.bci.demo.api.service.IApiUserPhoneService;

@Service
public class ApiUserPhoneServiceImp implements IApiUserPhoneService{

	@Autowired
	private IApiUserPhoneDao dao;
	
	@Override
	public void saveApiUserPhoneEntity(ApiUserPhoneEntity phone) {
		dao.save(phone);
	}

	@Override
	public List<ApiUserPhoneEntity> findByApiUserId(UUID id) {
		return dao.findByUserId(id);
	}

}
