package cl.edu.bci.demo.api.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public class ApiUserRequestBodyPhone {
	
	@Schema(description = "Número de teléfono", example = "1234567")
	private String number;
	@Schema(description = "Código de ciudad del teléfono", example = "1")
	private String cityCode;
	@Schema(description = "Código de País del teléfono", example = "57")
	private String countryCode;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
}
