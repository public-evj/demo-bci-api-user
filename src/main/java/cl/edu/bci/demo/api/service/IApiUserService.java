package cl.edu.bci.demo.api.service;

import java.util.List;
import java.util.UUID;

import cl.edu.bci.demo.api.model.dto.ApiUserLoginRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserLoginResponseBody;
import cl.edu.bci.demo.api.model.dto.ApiUserRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserResponseBody;
import cl.edu.bci.demo.api.model.entity.ApiUserEntity;

public interface IApiUserService {

	public List<ApiUserEntity> findAll();
	
	public ApiUserResponseBody saveApiUserEntity(final ApiUserRequestBody user);
	
	public ApiUserResponseBody findByApiUserId(final UUID id);
	
	public ApiUserLoginResponseBody login(final ApiUserLoginRequestBody login);
	
	
	

}
