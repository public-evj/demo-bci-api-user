package cl.edu.bci.demo.api.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.edu.bci.demo.api.model.dto.ApiUserLoginRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserLoginResponseBody;
import cl.edu.bci.demo.api.model.dto.ApiUserRequestBody;
import cl.edu.bci.demo.api.model.dto.ApiUserResponseBody;
import cl.edu.bci.demo.api.service.IApiUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("")
public class ApiUserController {
	
	@Autowired
	IApiUserService userService;
	
	@GetMapping(value = "/ping", produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@Operation(summary="Prueba la conexión con la API")
	@Tag(name = "Ping")
	public String ping() {
		return "pong";
	}
		
	@GetMapping(value = "/user/{id}")
	@ResponseStatus(HttpStatus.OK)
	@Operation(summary="Obtiene los datos de un usuario mediante el id.")
	@Tag(name = "Get User Data")
	public ApiUserResponseBody getUserById(@PathVariable("id") final String id) {
		return userService.findByApiUserId(UUID.fromString(id));
	}
	
	@PostMapping(value = "/user")
	@ResponseStatus(HttpStatus.CREATED)
	@Operation(summary="Registra un usuario en base de datos, incluyendo un listado con sus teléfonos.")
	@Tag(name = "Create User")
	public ApiUserResponseBody register(@Valid @RequestBody final ApiUserRequestBody request) {
		return userService.saveApiUserEntity(request);
	}
	
	@PostMapping(value = "/login")
	@ResponseStatus(HttpStatus.OK)
	@Operation(summary="Obtiene el id del usuario mediante email y la password.")
	@Tag(name = "Login User")
	public ApiUserLoginResponseBody login(@Valid @RequestBody final ApiUserLoginRequestBody request) {
		return userService.login(request);
	}
	
	
}
