package cl.edu.bci.demo.api.model.dto;

import java.util.Date;
import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;

public class ApiUserResponseBody {
	@Schema(description = "Usuario")
	private ApiUserRequestBody user;
	@Schema(description = "ID de usuario")
	private UUID id;
	@Schema(description = "Fecha de creación")
	private Date created;
	@Schema(description = "Fecha de última modificación")
	private Date modified;
	@Schema(description = "Fecha de último Acceso")
	private Date lastLogin;
	@Schema(description = "Token de usuario", example = "3fa85f64-5717-4562-b3fc-2c963f66afa6")
	private String token;
	@Schema(description = "Bandera que indica si el usuario está habilitado en el sistema")
	private boolean isActive;
	
	public ApiUserRequestBody getUser() {
		return user;
	}
	public void setUser(ApiUserRequestBody user) {
		this.user = user;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
}
