package cl.edu.bci.demo.api.exception;

public class ApiUserNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public ApiUserNotFoundException(){
		super("Usuario no encontrado");
	}
}
