package cl.edu.bci.demo.api.model.dto;

import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;

public class ApiUserLoginResponseBody {
	
	@Schema(description = "ID de usuario", example = "3fa85f64-5717-4562-b3fc-2c963f66afa6")
	private UUID id;

	public ApiUserLoginResponseBody(UUID id) {
		this.id = id;
	}
	public UUID getId() {
		return id;
	}
	
}
