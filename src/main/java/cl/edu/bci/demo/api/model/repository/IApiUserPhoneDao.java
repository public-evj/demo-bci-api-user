package cl.edu.bci.demo.api.model.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import cl.edu.bci.demo.api.model.entity.ApiUserPhoneEntity;

public interface IApiUserPhoneDao extends CrudRepository<ApiUserPhoneEntity, UUID>{

	public List<ApiUserPhoneEntity> findByUserId(UUID id);
}
