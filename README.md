# API RESTful Demo de Creación de Usuarios

_Esta API es una demo, la cual sirve para registrar usuarios. Proyecto desarrollado por Eduardo Vergara Jara_

## Comenzando 🚀

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

**_Descargue e Instale Gradle_**

_Siga las instrucciones de instalación desde el sitio oficial de Gradle según su sistema operativo. En el siguiente link podrá acceder a las instrucciones_


* [Gradle Install](https://gradle.org/install/)


**_Descargue e Instale Postman_**

_En el siguiente link encontrará el instalador de postman desde su sitio oficial._


* [Postman Install](https://www.postman.com/downloads/)



**_Clonar repositorio de la API_**

_Ejecute el siguiente comando._
```
git clone https://bitbucket.org/public-evj/demo-bci-api-user.git
```

### Instalación 🔧

_Luego de clonar el repositorio posicionese dentro del directorio **src/main/resources/** y edite el archivo **application.properties**_


_Configure la expresión regular que se utilizará para validar la password en el registro de usuarios, modificando la propiedad **password.constraint.regex**_

```
password.constraint.regex=^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,12}$
```


_Además asegurese de configurar el mensaje que se mostrará cuando no se cumpla la validación, modificando la propiedad **password.constraint.error.message**_

```
password.constraint.error.message=debe tener entre 6 y 12 caracteres, al menos 1 mayúscula, almenos 1 minúscula, almenos 1 número y el único símbolo aceptado es guión bajo
```


_Una vez configurado vuelva al directorio raiz del proyecto_


## Despliegue 📦

_Despliegue la API utilizando el siguiente comando_

```
gradle bootRun
```
## Documentación Swagger OAS3 📖

_Esta API tiene embebida la documentación Swagger, generada desde el código fuente._ 

_Para ver interfaz gráfica de esta documentación acceda al siguiente link en el navegador_

* [swagger/ui](http://localhost:8080/apiDemoBci/swagger/ui)

_Para obtener documentación en formato JSON acceda al siguiente link_

* [swagger/api-docs](http://localhost:8080/apiDemoBci/swagger/api-docs)


## Ejecutando las pruebas ⚙️

_Importar los 2 archivos postman que se encuentran dentro del directorio **/postman** en el repositorio_

```
ApiUserDemoCollection.postman_collection.json
ApiUserDemoLocalEnv.postman_environment.json
```

_Los archivos postman están configurados para asignar automaticamente las variables, por lo que podrá incialmente realizar pruebas en cada endpoint sin modificar nada. Luego puede moficar los campos para probar la API ante diferentes peticiones._

_Seleccionar el environment de postman importado_

### La API posee 4 endpoints

#### PING -> /ping (GET)
_Sirve para probar la conexión con la API. Este endpoint responderá con el texto "pong" cuando la conexión es correcta_

![Ping](images/ping.PNG?raw=true "/ping")

#### Create User -> /user (POST)
_Sirve para registrar un usuario en base de datos, incluyendo un listado con sus teléfonos. Este endpoint responde con los datos del usuario creado más algunos datos de configuración de este._

![Create User](images/createUser.PNG?raw=true "/user")

#### Login User -> /login (POST)
_Sirve para obtener el **id** del usuario, el cual se utilizará para poder acceder a los datos de éste. Este endpoint es una simulación de login de usuario y recibe el email y la password._

![Login](images/login.PNG?raw=true "/login")

#### Get User Data -> /user/{id} (GET)
_Sirve para obtener los datos de un usuario utilizando el id de este, el cual se obtiene en el endpoint de login._

![Get User](images/getUser.PNG?raw=true "/user/{id}")

_La mayoría de los errores están controlados, por lo que podrá realizar pruebas fallidas para poder probar el control de excepciones de la API._

## Herramientas utilizadas 🛠️

* [Eclipse](https://www.eclipse.org/) - IDE
* [Spring Boot](https://spring.io/projects/spring-boot) - Framework
* [Gradle](https://gradle.org/) - Build tool
* [Postman](https://www.postman.com/) - Api Platform

## Autor ✒️

* **Eduardo Vergara Jara**